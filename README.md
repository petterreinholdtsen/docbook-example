Example docbook source
======================

Created to demonstrate use of various strings that can be translated
in Docbook.  Change code used in 'lang=' setting at the top of the XML
files to test different languages.

This also demonstrate the default output of various Docbook processors.

* Example book
   * [dblatex PDF](output/examplebook-dblatex.pdf)
   * [dbtoepub epub](output/examplebook.epub)
   * [xmlto HTML](output/examplebook.html)
   * [XSLT/FOP PDF](output/examplebook.pdf)
- Example book set
   * [dblatex PDF](output/exampleset-dblatex.pdf)
   * [dbtoepub ePub](output/exampleset.epub)
   * [xmlto HTML](output/exampleset.html)
   * [XSLT/FOP PDF](output/exampleset.pdf)
- Example book with articles
   * [dblatex PDF](output/example-articlebook-dblatex.pdf) - missing, build failed
   * [dbtoepub ePub](output/example-articlebook.epub)
   * [xmlto HTML](output/example-articlebook.html)
   * [XSLT/FOP PDF](output/example-articlebook.pdf)
- Example book with mostly tables
   * [dblatex PDF](output/tables-dblatex.pdf) - missing, build failed
   * [dbtoepub ePub](output/tables.epub)
   * [xmlto HTML](output/tables.html)
   * [XSLT/FOP PDF](output/tables.pdf)
- Example book with sidebar
   * [dblatex PDF](output/sidefloat-dblatex.pdf)
   * [dbtoepub ePub](output/sidefloat.epub)
   * [xmlto HTML](output/sidefloat.html)
   * [XSLT/FOP PDF](output/sidefloat.pdf)
The output was generated on Debian 10 Buster.

The latest update of this example is available from
https://codeberg.org/pere/docbook-example .
