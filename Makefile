SOURCE = examplebook.xml exampleset.xml

BUILD = \
  example-articlebook.pdf example-articlebook.epub example-articlebook.html example-articlebook.xsl.html \
  examplebook.pdf examplebook.epub examplebook.html examplebook.xsl.html \
  exampleset.pdf exampleset.epub exampleset.html exampleset.xsl.html \
  examplebook5.pdf examplebook5.epub examplebook5.html examplebook5.xsl.html \
  sidefloat.pdf sidefloat.epub sidefloat.html sidefloat.xsl.html \
  tables.pdf tables.epub tables.html tables.xsl.html

all: $(BUILD)

.xml.pdf:
	xsltproc  \
	    --output $(subst .pdf,.fo,$@) \
	    /usr/share/xml/docbook/stylesheet/docbook-xsl/fo/docbook.xsl \
	    $< ; \
	  fop -fo $(subst .pdf,.fo,$@) -pdf $@ ; \
	  #rm $(subst .pdf,.fo,$@)
	dblatex $< -o $(subst .pdf,-dblatex.pdf,$@)

.xml.html:
	xmlto html-nochunks $<

.xml.xsl.html:
	xsltproc  \
	    --output $(subst .pdf,.fo,$@) \
	    /usr/share/xml/docbook/stylesheet/docbook-xsl/html/docbook.xsl \
	    $<

.xml.epub:
	dbtoepub $< -o $@

XMLLINTOPTS = --nonet --noout  --xinclude --postvalid 
lint: $(SOURCE)
	xmllint $(XMLLINTOPTS) $^

clean:
	$(RM) *~
	$(RM) $(BUILD)
distclean: clean

.SUFFIXES: .xml .html .pdf .epub .xsl.html
